FROM fedora:30 

RUN dnf install -y python3-pip git-core

RUN pip3 install --upgrade python-gitlab imapclient configparser

RUN git clone https://gitlab.com/nhorman/git-lab-porcelain.git

RUN chmod 0755 git-lab-porcelain/emaillistener.py

ADD ./run_listener.sh /
RUN chmod 0755 /run_listener

ENTRYPOINT ["/run_listener"]
