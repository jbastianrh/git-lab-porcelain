#!/bin/sh

set -x

pip install --upgrade python-gitlab gitpython imapclient configparser

export PYTHONPATH=$PYTHONPATH:/opt/app-root/lib/python3.6/site-packages/

echo $gitlabemailconfig > ./environ
. ./environ
rm -f ./environ


# Create our config file
env echo "[email]" > ./config
env echo "	server = " $CFG_EMAIL_SERVER >> ./config
env echo "	account = " $CFG_EMAIL_ACCOUNT >> ./config
env echo "	password = " $CFG_EMAIL_PASSWORD >> ./config
env echo "[gitlab]" >> ./config
env echo "	url = " $CFG_GITLAB_URL >> ./config
env echo "	key = " $CFG_GITLAB_KEY >> ./config
env echo "[policy]" >> ./config
env echo "	ackcount = " $CFG_POLICY_ACKCOUNT >> ./config
env echo "	nackcount = " $CFG_POLICY_NACKCOUNT >> ./config

/opt/app-root/bin/python3 ./emaillistener.py ./config
