%global date    %(date +%%Y%%m%%d)

Name:    git-lab-porcelain 
Version: 0 
Release: %{date}git%{shortcommit}%{?dist}
Summary: Git porcelain for working with gitlab 
BuildArch: noarch

Requires: python3-gitlab
Requires: python3-GitPython

License: GPLv2
URL:     https://gitlab.com/nhorman/git-lab-porcelain 

Source0: %{name}-%{shortcommit}.tar.gz

%description
A porcelain for git to facilitate command line creation/listing/editing and
reviewing of merge requests in gitlab

%prep
%autosetup -n git-lab-porcelain-%{shortcommit}


%build
# nothing to do here

%install
mkdir -p $RPM_BUILD_ROOT/%{_libexecdir}/
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1/
install -m 0755 git-lab $RPM_BUILD_ROOT/%{_libexecdir}/git-lab
install -m 0644 git-lab.1 $RPM_BUILD_ROOT/%{_mandir}/man1/git-lab.1

%files
%{_libexecdir}/*
%{_mandir}/man1/*

%changelog
* Tue Oct 22 2019 Neil Horman <nhorman@redhat.com> - 0-1
- Initial release
