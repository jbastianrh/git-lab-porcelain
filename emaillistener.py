#################################################################
# Email monitor for email list to gitlab translation
#
# This script monitors a specified email list for incoming emails
# And, based on the content of the body takes appropriate action
# on the gitlab merge request that the email refers to
#
# Relevant email format
# This script will process any email that has an email at the root of its
# reply-to chain which contains a Message-Id in the format "<merge-request.*".
# For any email matching that criteria the X-RH-GITLAB-PROJECT and
# X-RH-GITLAB-MERGEID headers will be extracted and used as correlators to the
# appropriate gitlab merge rquest.  Then, based on the following detected patterns
# in the email body:
# Acked-by: <user>
# Nacked-by: <user>
# Ack-Rescinded-by: <user>
# Nack-Rescinded-by: <user>
# This script will update the merge request with a relevant label recording, or
# removing the appropriate ACK/NACK information.
# After every update, the merge request will be queried for all its labels, and if
# sufficent acks have been granted, the merge request will be approved for merging
# 
# Requirements
# To operate properly, this script must have access to the specified gitlab
# instance as a user who is granted permission to approve/disapprove merge
# requests for the specified project
# 
# Usage
# emaillistener.py <configfile>
# Where configfile contains:
#
#[email]
# server = <server ip>
# account = <username>
# password = <password>
#[gitlab]
# url = <gitlab server url for rest api
# key = <gitlab user authentication token>
#[policy]
# ackcount = <number of acks to approve a merge
# nackcount = <number of nacks that prevents a merge
#################################################################


import json
import time
import gitlab
import imapclient
import ssl
import email
import re
import os
import sys
import subprocess
import configparser


# gitlab account info
gitlaburl = None
gitlabkey = None

# Other global variables
context = None 
imap = None
ackcount = None
nackcount = None

###################################################################
# Class to parse and execute actions found in a matching email body
###################################################################
class Action():
	def __init__(self, response_email, root_email):
		global gitlaburl
		global gitlabkey
		global ackcount
		global nackcount

		self.ACKCOUNT = ackcount
		self.NACKCOUNT = nackcount
		self.response_email = response_email
		self.root_email = root_email
		self.lab = gitlab.Gitlab(gitlaburl, gitlabkey)
		self.action = "unknown"
		self.user = None
		body = response_email.get_payload()
		self.user = response_email.get('From')

		#get just the user portion of the From line, as gitlab doesn't want non alpha chars in the label
		idx = self.user.index('<')
		self.user = self.user[idx:]
		self.user=self.user.strip('<').strip('>')
		idx=self.user.index("@redhat.com")
		self.user=self.user[:idx]

		self.patch = root_email.get('X-RH-GITLAB-PATCH')
		self.project = int(root_email.get('X-RH-GITLAB-PROJECT'))
		self.mergeid = int(root_email.get('X-RH-GITLAB-MERGEID'))
		subject = root_email.get('Subject')
		subject = re.search("([0-9]+)(\/)([0-9]+)(\])", subject)
		self.patchidx = int(subject.group(1))
		self.patchcount = int(subject.group(3))
		m = re.search("^Acked-By:", body, re.IGNORECASE|re.MULTILINE)
		if m:
			self.action = "ACK"
		else:
			m = re.search("^Nacked-By:", body, re.IGNORECASE|re.MULTILINE)
			if m:
				self.action = "NACK"


		self.lab.auth()
		try:
			print("Getting project %s" % self.project)
			self.project = self.lab.projects.get(self.project)
		except Exception as e:
			print("Unable to find project %s:%s" % (self.project, e))
			return

		try:
			self.mr = self.project.mergerequests.get(self.mergeid, lazy=False)
		except Exception as e:
			print("Unable to find merge request %d:%s" % (self.mergeid, e))
			return
		return

	def getActions(self):
		self.actions = []
		body = self.response_email.get_payload()
		for line in body.splitlines():
			if re.search("^Acked-By:", line, re.IGNORECASE|re.MULTILINE):
				#self.mr.labels = self.mr.labels + [self.user+":ACK"]
				self.actions.append(['add', 'ack', self.user])
			if re.search("^Nacked-By:", line, re.IGNORECASE|re.MULTILINE):
				#self.mr.labels = self.mr.labels + [self.user+":NACK"]
				self.actions.append(['add', 'nack', self.user])
			if re.search("^Ack-Rescinded-By:", line, re.IGNORECASE|re.MULTILINE):
				#remlabel = self.user+":ACK"
				#try:
				#	print("Removing ACK")
				#	self.mr.labels.remove(remlabel)
				#except Exception:
				#	print("Skipping removal of non-existant ACK label")
				self.actions.append(['remove', 'ack', self.user])
			if re.search("^Nack-Rescinded-By:", line, re.IGNORECASE|re.MULTILINE):
				#remlabel = self.user+":NACK"
				#try:
				#	self.mr.labels.remove(remlabel)
				#except Exception:
				#	print("Skipping removal of non-existant NACK label")
				self.actions.append(['remove', 'nack', self.user])
		#self.mr.labels = list(dict.fromkeys(self.mr.labels))
	
	def getLatestMetadata(self):
		metadata = None
		note = None
		for d in self.mr.discussions.list():
			dobj = self.mr.discussions.get(d.id)
			for n in dobj.attributes['notes']:
				nobj = json.loads(n['body'])
				if (metadata == None) or (metadata['Revision'] < nobj['Revision']):
					metadata = nobj
					note = dobj.notes.get(n['id']) 

		return (note, metadata)

	def executeAction(self):
		self.lab.auth()
		#print(self.mr.labels)
		#labelstring = ""
		#for i in self.mr.labels:
		#	labelstring = labelstring + i + ","
		#labelstring.rstrip(',')
		#curlcmd = 'curl -s i -X PUT --header "Private-Token: 3xeRjQn2MA4NEsxLSbtS" -d "labels='+labelstring+'" https://gitlab.com/api/v4/projects/' + str(self.project.id) + '/merge_requests/' + str(self.mr.iid)
		#result = subprocess.run(curlcmd, capture_output=True, shell=True)
		#result.stdout = json.loads(result.stdout)	
		(nobj, metadata) = self.getLatestMetadata()
		self.metadata = metadata
		for a in self.actions:
			user = a[2]
			if a[1] == 'ack':
				mlist = self.metadata['ACKS']
			else:
				mlist = self.metadata['NACKS']
			if a[0] == 'add':
				mlist.append(self.user)
			else:
				try:
					mlist.remove(self.user)
				except valueError as e:
					print(e)

		self.metadata['Revision'] = self.metadata['Revision']+1
		nobj.body = json.dumps(metadata)
		nobj.save()
		self.checkAndApprove()
		
	def checkAndApprove(self):
		ackcount = 0
		nackcount = 0
		#for l in self.mr.labels:
		#	tag = l.split(':')
		#	if tag[1] == "ACK":
		#		ackcount = ackcount + 1
		#	else:
		#		nackcount = nackcount + 1
		ackcount = int(len(self.metadata['ACKS']))
		nackcount = int(len(self.metadata['NACKS']))
		print("ACK=%d, NACKS=%d" % (ackcount, nackcount))
		if ackcount >= self.ACKCOUNT  and nackcount == self.NACKCOUNT:
			# gitlab-python doesn't have an api to handle merge request approvals, so we have to use a raw curl command here
			systemcmd='curl -s -X POST --header "Private-Token: 3xeRjQn2MA4NEsxLSbtS" https://gitlab.com/api/v4/projects/' + str(self.project.id) + '/merge_requests/' + str(self.mr.iid) + '/approve' 
			result = subprocess.run(systemcmd, capture_output=True, shell=True)
		elif nackcount != 0:
			systemcmd='curl -s -X POST --header "Private-Token: 3xeRjQn2MA4NEsxLSbtS" https://gitlab.com/api/v4/projects/' + str(self.project.id) + '/merge_requests/' + str(self.mr.iid) + '/unapprove' 
			result = subprocess.run(systemcmd, capture_output=True, shell=True)
		

#############################################################
# Helper function to get our merge id
#############################################################
def get_merge_id(root_email):
	message_id = root_email.get('X-RH-GITLAB-MERGEID')
	print ("Merge id is %d" % int(message_id))
	return message_id

###############################################################
# Function to update gitlab with information in the response email
###############################################################
def update_gitlab(response_email, root_email):
	# Class to parse the action items out of the emails
	# Creating an instance of the class gets the appropriate meta
	# data from the root email
	action = Action(response_email, root_email)
	# Parse the actions out of the bosy
	action.getActions()
	# And update gitlab with those actions
	action.executeAction()

##################################################################
# given an arbitrary email, find the first parent email in the thread that starts with a merge_request
# Message-Id.  This will find the root email of that thread, which contains the project and merge request ids
# If no such message is found, it returns the current message
#################################################################
def trace_to_root(current_msg, level=0):
	# get the current message reply to id
	reply_id = current_msg.get('In-Reply-to')
	if (reply_id == None):
		print("Message with no reply-to")
		return current_msg
	reply_id = reply_id.strip('<').strip('<')

	# Get the current message id
	msg_id = current_msg.get('Message-Id')

	# if the current message is a merge_request message, its not a reply, so skip it
	result = re.search('^<merge_request.*$', msg_id)
	if (result == None):
		# If its not a match then its possibly a reply to a patch email
		# find all the messages that have message id matching the reply id
		# Should only be one
		messages = imap.search(['HEADER', 'Message-Id', reply_id])
		if (messages == None):
			print("No Parent Messages")
			return current_msg
		for uid, message_data in imap.fetch(messages, 'RFC822').items():
			print("Checking message %d at level %d" % (uid, level))
			current_msg = email.message_from_bytes(message_data[b'RFC822'])
			# Recursively call ourselves to trace back through the thread
			return trace_to_root(current_msg, level + 1)
	return current_msg
		
#########################################################################
# Loop until ctrl-c is pressed, checking for new messages on list every 10s
########################################################################
def serverloop():
	try:
		while True:
			imap.select_folder('INBOX')
			# look for all new emails
			print("Checking Email")
			messages = imap.search(['UNSEEN'])
			print(messages)
			if (messages == None):
				continue
			print("Checking new messages")
			# We found new emails, look at each to identify if they are merge requests
			for uid, message_data in imap.fetch(messages, 'RFC822').items():
				email_msg = email.message_from_bytes(message_data[b'RFC822'])
				print(email_msg)
				# Messages with an X-Loop header are duplicates and can be skipped
				if (email_msg.get('X-Loop') != None):
					print("Skipping duplicate message")
					continue

				# This function finds the first non-reply email in the thread belonging to the new email
				# That root email contains headers mapping this thread to a project and merge request id
				root_email = trace_to_root(email_msg)
				print("%s : %s" % (email_msg.get('Message-Id'), root_email.get('Message-Id')))

				# If the new email is in fact the root of the thread, then we can skip it, as there will
				# be no ack/nack commands in the body
				if (email_msg.get('Message-Id') == root_email.get('Message-Id')):
					# The email_msg is the root email, nothing to do here
					print ("Root message patch email, skipping")
					continue

				# Likewise, if the email we get has a message id that starts with
				# "<merge_request", its a patch email and can be skipped
				if email_msg.get('Message-Id').startswith('<merge_request'):
					# This is a patch email, skip it
					print ("This is a patch email, skipping")
					continue

				# If the root email starts with a merge request id, then this is a response to a 
				# patch email and we need to process it
				if root_email.get('Message-Id').startswith('<merge_request'):		
					update_gitlab(email_msg, root_email)
				else:
					print("This is not a review thread")

			time.sleep(10)
	except KeyboardInterrupt:
		return



def main():
	config = configparser.ConfigParser()
	config.read(sys.argv[1])
	
	global imap
	global context
	global gitlaburl
	global gitlabkey
	global ackcount
	global nackcount

	context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
	context.verify_mode = ssl.CERT_NONE
	imap = imapclient.IMAPClient(config['email']['server'], ssl=True,ssl_context=context)
	imap.login(config['email']['account'], config['email']['password'])
	gitlaburl = config['gitlab']['url']
	gitlabkey = config['gitlab']['key']
	ackcount = int(config['policy']['ackcount'])
	nackcount = int(config['policy']['nackcount'])
	serverloop()
	imap.logout()

if __name__ == "__main__":
    main()

